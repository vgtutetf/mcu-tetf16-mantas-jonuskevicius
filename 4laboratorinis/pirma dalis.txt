int main(void){ /* Reset of all peripherals, Initializes the Flash interface and the Systick. */ 
  HAL_Init(); /* Configure the system clock */ 
  SystemClock_Config(); /* Initialize all configured peripherals */  
  //  MX_GPIO_Init();    
  // specialiai uzkomentaves, kad konfiguravimo darbus atliktume     
  // rankiniu budu 
  // GPIO strukturos inicializavimas 
  GPIO_InitTypeDef GPIO_InitStruct; 
  // Enable the GPIOD Clock 
  __HAL_RCC_GPIOD_CLK_ENABLE(); 
  __HAL_RCC_GPIOA_CLK_ENABLE(); 
 
// GPIOD Configuration 
  GPIO_InitStruct.Pin = GPIO_PIN_12 ; 
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP; 
  GPIO_InitStruct.Pull = GPIO_NOPULL; 
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW; 
  //Initialize pins of Port D 
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct); 
 
// User button on PORT A Pin 0 
  GPIO_InitStruct.Pin = GPIO_PIN_0; 
  //Mode output 
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT; 
  //Output type push-pull with pull down resistor 
  GPIO_InitStruct.Pull = GPIO_PULLDOWN; 
  //50MHz pin speed 
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM; 
  //Initialize pins 
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct); 
 
while (1){     /* USER CODE END WHILE */     
 HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12); 
HAL_Delay(1000); 
 //Toggle the state of pin PC12     
  
 
//HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);  
 if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0)){   
  // checks if PA0 is set   
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);  
  }else{   
   HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET);  
   }
  HAL_Delay(100); //delay 100ms   
   } 
 }  
